﻿using System;
using System.IO;
using DrawingViewAlignerApp;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Tekla.Structures.Drawing;

namespace Testing
{
    [TestClass]
    public class TestDrawingViewAligner
    {
        [TestMethod]
        public void RunOnce()
        {

            var drawingHandler = new DrawingHandler();
            var drawingViewAligner = new DrawingViewAligner();

            if (drawingHandler.GetConnectionStatus())
            {
                Drawing activeDrawing = drawingHandler.GetActiveDrawing();
                drawingViewAligner.Align(activeDrawing);

            }
        }
    }

    [TestClass]
    public class TestPrintParts
    {
        [TestMethod]
        public void PrintAllParts()
        {
            var drawingHandler = new DrawingHandler();

            if (drawingHandler.GetConnectionStatus())
            {
                Drawing activeDrawing = drawingHandler.GetActiveDrawing();
                var sheet = activeDrawing.GetSheet();
                var objects = sheet.GetObjects();

                DateTime timestamp = System.DateTime.Now;


                string filePath = @".\dump" + timestamp.ToString("yyyyMMddHHmmss") + ".json";
                Console.WriteLine(filePath);

                while (objects.MoveNext())
                {
                    string json = JsonConvert.SerializeObject(objects.Current, Formatting.Indented);
                    File.AppendAllText(filePath, json);
                }
            }
        }
    }

}
