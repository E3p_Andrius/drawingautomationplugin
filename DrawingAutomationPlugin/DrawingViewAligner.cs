﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tekla.Structures.Drawing;

namespace DrawingViewAlignerApp
{
    public class DrawingViewAligner
    {
        public void Align(Drawing drawing)
        {
            ContainerView containerView = drawing.GetSheet();
            DrawingObjectEnumerator views = containerView.GetAllViews();

            while (views.MoveNext())
            {
                if(views.Current is View currentView)
                {
                    currentView.Origin.X -= 100;
                    currentView.Modify();
                }
            }
        }
    }
}
