﻿using DrawingViewAlignerApp;
using System;
using System.Collections.Generic;
using Tekla.Structures.Drawing;
using Tekla.Structures.Plugins;

namespace DrawingAutomationPlugin
{
    [Plugin("DrawingAutomationPlugin")]
    [PluginUserInterface("DrawingAutomationPlugin.MainForm")]
    public class DrawingAutomationPlugin : DrawingPluginBase
    {

        public DrawingAutomationPlugin()
        {
            drawingHandler = new DrawingHandler();
            drawingViewAligner = new DrawingViewAligner();
        }

        private readonly DrawingHandler drawingHandler;
        private readonly DrawingViewAligner drawingViewAligner;
        public override List<InputDefinition> DefineInput()
        {
            var inputDefinitions = new List<InputDefinition>();
            return inputDefinitions;
        }

        public override bool Run(List<InputDefinition> Input)
        {
            try
            {
                if (drawingHandler.GetConnectionStatus()) 
                {
                    Drawing activeDrawing = drawingHandler.GetActiveDrawing();
                    drawingViewAligner.Align(activeDrawing);

                }
            }
            catch (Exception)
            {
                throw;
            }

            return true;
        }
    }
}
